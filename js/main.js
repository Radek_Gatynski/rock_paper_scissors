let userScore = 0;
let aiScore = 0;
let noWinScore = 0;

const userScoreSpan = document.getElementById("user-score");
const aiScoreSpan = document.getElementById("ai-score");
const scoreBoard = document.querySelector(".score-board");
const resultDiv = document.querySelector(".result");
const rock_span = document.getElementById("rock");
const paper_span = document.getElementById("paper");
const scissor_span = document.getElementById("scissor");

const saveScore = document.getElementById("btn-save");
const loadScore = document.getElementById("btn-load");
const clearScore = document.getElementById("btn-clear");

const infoMess = document.getElementById("pointChoiceInfo");


function getAIchoice() {
    const choice = ['r', 'p', 's'];
    const randomChoice = Math.floor(Math.random() * 3);
    return choice[randomChoice];
}

function returnOption(option) {
    let optionReturn = "";
    switch (option) {
        case "r":
            optionReturn = "Rock";
            break;
        case "s":
            optionReturn = "Scissor";
            break;
        case "p":
            optionReturn = "Paper";
            break;
    }
    return optionReturn;
}

function win(userCh, aiChoice) {
    console.log("User win");
    userScore++;
    userScoreSpan.innerText = userScore;
    resultDiv.innerHTML = "<p>USER WIN. His choice: " + returnOption(userCh) + " AI choice: " + returnOption(aiChoice) + "</>";

}

function lose(userCh, aiChoice) {
    console.log("AI win");
    aiScore++;
    aiScoreSpan.innerText = aiScore;
    resultDiv.innerHTML = "<p>AI WIN. His choice: " + returnOption(aiChoice) + " User choice: " + returnOption(userCh) + "</p>";

}

function noWin(userCh) {
    console.log("NO winers");
    noWinScore++;
    resultDiv.innerHTML = "<p>No WIN, choice: " + returnOption(userCh) + "</p>";

}

function putToLocalStorage(userPoint, aiPoint) {
    localStorage.setItem('userPoints', userPoint);
    localStorage.setItem('aiPoints', aiPoint);
    infoMess.innerText = "Points saved in browser";
}

function getFromLocalStorage() {
    if (localStorage.getItem("aiPoints") === null || localStorage.getItem("userPoints") === null) {
        console.log("No storage");
        infoMess.innerText = "No points to load"
        return false;
    } else {
        aiScore = localStorage.getItem('aiPoints');
        userScore = localStorage.getItem('userPoints');
        aiScoreSpan.innerText = aiScore;
        userScoreSpan.innerText = userScore;
        infoMess.innerText = "Points loaded"
    }
}

function clearAllScore() {
    aiScore = 0;
    userScore = 0;
    userScoreSpan.innerText = userScore;
    aiScoreSpan.innerText = aiScore;
    localStorage.removeItem("userPoints");
    localStorage.removeItem("aiPoints");
    infoMess.innerText = "Points removed";
}

function game(userChoice) {
    const computerChoice = getAIchoice();
    switch (userChoice + computerChoice) {
        case "pr":
        case "sp":
        case "rs":
            win(userChoice, computerChoice);
            break;
        case "rp":
        case "sr":
        case "ps":
            lose(userChoice, computerChoice);
            break;
        case "rr":
        case "pp":
        case "ss":
            noWin(userChoice, computerChoice);
            break;
    }
}

function main() {
    rock_span.addEventListener('click', function () {
        game("r");
    });
    paper_span.addEventListener('click', function () {
        game("p");
    });
    scissor_span.addEventListener('click', function () {
        game("s");
    });
    saveScore.addEventListener('click', function () {
        putToLocalStorage(userScore, aiScore);
    });
    loadScore.addEventListener('click', function () {
        getFromLocalStorage();
    });
    clearScore.addEventListener('click', function () {
        clearAllScore();
    });
}
main();