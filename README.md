## ROCK PAPER SCISSOR
Game between user and AI.

## Used technology
### HTML, SCSS, JS

## How run
Open index.html in your browser, also we need have internet connection to be able to load fonts.

## How it's works

The game can store the score in localStorage and when we visit the page again we can load the last result. 
When we don't have any score saved in Storage we will get information about it.
We can also delete the score and save the score.
All this function we have under the buttons.

